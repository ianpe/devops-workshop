package resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Calculator resource exposed at '/calculator' path
 */
@Path("/calculator")
public class CalculatorResource {

    /**
     * Method handling HTTP POST requests. The calculated answer to the expression will be sent to the client as
     * plain/text.
     * @param expression the expression to be solved as plain/text.
     * @return solution to the expression as plain/text or -1 on error
     */
    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public String calculate(String expression){
        // Removes all whitespaces
        String expressionTrimmed = expression.replaceAll("\\s+","");
        String s = null;
        int result = -1;

        /*
         * .matches use regex expression to decide if a String matches a given pattern.
         * [0-9]+[+][0-9]+ explained:
         * [0-9]+: a number from 0-9 one or more times. For example 1, 12 and 123.
         * [+]: the operator + one time.
         * The total regex expression accepts for example:
         * 12+34,
         * 1+2,
         * 10000+1000
         */
        if(expressionTrimmed.matches("[0-9]+[+][0-9]+")) result = sum(expressionTrimmed);
        else if(expressionTrimmed.matches("[0-9]+[-][0-9]+")) result = subtraction(expressionTrimmed);
        else if(expressionTrimmed.matches("[0-9]+[*][0-9]+")) result = multiplication(expressionTrimmed);
        else if(expressionTrimmed.matches("[0-9]+[/][0-9]+")) result = division(expressionTrimmed);
        else if(expressionTrimmed.matches("^\\d+(?:\\s*[+]\\s*\\d+)*$")) result = sum2(expressionTrimmed);
        else if(expressionTrimmed.matches("^\\d+(?:\\s*[-]\\s*\\d+)*$")) result = subtraction2(expressionTrimmed);
        else if(expressionTrimmed.matches("^\\d+(?:\\s*[*]\\s*\\d+)*$")) result = multiplication2(expressionTrimmed);
        else if(expressionTrimmed.matches("^\\d+(?:\\s*[/]\\s*\\d+)*$")) result = division2(expressionTrimmed);

        if (result == -1){
            return s = "Invalid input";
        } else {
            return String.valueOf(result);
        }
    }

    /**
     * Method used to calculate a sum expression.
     * @param expression the equation to be calculated as a String
     * @return the answer as an int
     */
    public int sum(String expression){
        String[] split = expression.split("[+]");

        int number1 = Integer.parseInt(split[0]);
        int number2 = Integer.parseInt(split[1]);

        return number1 + number2;
    }

    /**
     * Method used to calculate a subtraction expression.
     * @param expression the expression to be calculated as a String
     * @return the answer as an int
     */
    public int subtraction(String expression){
        String[] split = expression.split("[-]");

        int number1 = Integer.parseInt(split[0]);
        int number2 = Integer.parseInt(split[1]);

        return number1 - number2;
    }

    /**
     * Method used to calculate a multiplication equation.
     * @param equation the equation to be calculated as a String
     * @return the answer as an int
     */
    private int multiplication(String equation){
        String[] split = equation.split("[*]");

        int number1 = Integer.parseInt(split[0]);
        int number2 = Integer.parseInt(split[1]);

        return number1 * number2;
    }

    /**
     * Method used to calculate a division equation.
     * @param equation the equation to be calculated as a String
     * @return the answer as an int
     */
    private int division(String equation){
        String[] split = equation.split("[/]");

        int number1 = Integer.parseInt(split[0]);
        int number2 = Integer.parseInt(split[1]);

        return number1 / number2;
    }

    /**
     *  Method used to calculate a sum expression with 3 integers
     * @param expression the equation to be calculated as a String
     * @return the answer as an int
     */
    private int sum2(String expression){
        String[] split = expression.split("[+]");

        int number1 = Integer.parseInt(split[0]);
        int number2 = Integer.parseInt(split[1]);
        int number3 = Integer.parseInt(split[2]);

        return number1 + number2 + number3;
    }

    /**
     *  Method used to calculate a subtraction expression with 3 integers
     * @param expression the expression to be calculated as a String
     * @return the answer as an int
     */
    private int subtraction2(String expression){
        String[] split = expression.split("[-]");

        int number1 = Integer.parseInt(split[0]);
        int number2 = Integer.parseInt(split[1]);
        int number3 = Integer.parseInt(split[2]);

        return number1 - number2 - number3;
    }

    /**
     *  Method used to calculate a multiplication equation with 3 integers
     * @param equation the equation to be calculated as a String
     * @return the answer as an int
     */
    private int multiplication2(String equation){
        String[] split = equation.split("[*]");

        int number1 = Integer.parseInt(split[0]);
        int number2 = Integer.parseInt(split[1]);
        int number3 = Integer.parseInt(split[2]);

        return number1 * number2 * number3;
    }

    /**
     *  Method used to calculate a division equation with 3 integers
     * @param equation the equation to be calculated as a String
     * @return the answer as an int
     */
    private int division2(String equation){
        String[] split = equation.split("[/]");

        int number1 = Integer.parseInt(split[0]);
        int number2 = Integer.parseInt(split[1]);
        int number3 = Integer.parseInt(split[2]);

        return number1 / number2 / number3;
    }
}
