import org.junit.Test;
import resources.CalculatorResource;

import static org.junit.Assert.assertEquals;

public class CalculatorResourceTest{

    @Test
    public void testCalculate(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        assertEquals("400", calculatorResource.calculate(expression));

        expression = " 300 - 99 ";
        assertEquals("201", calculatorResource.calculate(expression));

        expression = " 2 * 50 ";
        assertEquals( "100", calculatorResource.calculate(expression));

        expression = " 100 / 2";
        assertEquals("50", calculatorResource.calculate(expression));

        expression = " 2 + 2 + 2 ";
        assertEquals("6", calculatorResource.calculate(expression));

        expression = " 2 * 2 * 2 ";
        assertEquals("8", calculatorResource.calculate(expression));

        expression = " 16 / 2 / 2 ";
        assertEquals("4", calculatorResource.calculate(expression));

        expression = " 8 - 2 - 2 ";
        assertEquals("4", calculatorResource.calculate(expression));
    }

    @Test
    public void testSum(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        assertEquals(400, calculatorResource.sum(expression));

        expression = "300+100";
        assertEquals(400, calculatorResource.sum(expression));
    }

    @Test
    public void testSubtraction(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "999-100";
        assertEquals(899, calculatorResource.subtraction(expression));

        expression = "20-2";
        assertEquals(18, calculatorResource.subtraction(expression));
    }
}
